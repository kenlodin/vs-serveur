/*
 * Client.cpp
 *
 *  Created on: 18 avr. 2012
 *      Author: nagriar
 */

#include <core/client/Client.hh>

Client::Client( boost::asio::io_service& ioService)
    : privilegeLevel_(10), publicIp_(""), privateIp_(""), token_("")
    , isActiv_(true)
{
    
    sockets_.push_back(new boostIp::tcp::socket(ioService));
}

Client::~Client()
{
    for (boost_socket* sock : sockets_)
    {
        sock->cancel();
        sock->close();
	delete sock;
    }
    //TODO delete current action
    using_.unlock();
}

int Client::send(Packet& packet)
{
    for (boost_socket* sock : sockets_)
    {
        if (packet.GetDataSize() == boost::asio::write(*sock, boost::asio::buffer(packet.GetData())))
	  return RETURN_VALUE_GOOD;
    }
    return RETURN_VALUE_ERROR;
}
